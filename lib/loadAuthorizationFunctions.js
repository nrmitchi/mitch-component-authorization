
var requireDir = require('require-directory');
var path = require('path');
var NOOP = function () {};
var _ = require('lodash');
var debug = require('debug')('mitch:loadServices');

var authorizationDir = 'authorization';

module.exports = function (app) {

  var loaded;

  var visitor = function(obj) {
    return obj(app);
  };

  try {
    loaded = requireDir(module, path.join(app.baseDir, authorizationDir)); // , { visit: visitor });
  } catch (e) {
    // Typical case of getting here is that the dir doesn't exist
    loaded = {
      registry: {},
      acls: {}
    };
  }
  
  return loaded;

};
