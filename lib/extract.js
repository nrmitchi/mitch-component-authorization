
/**
 * Extracts the model name from the request
 */
var mapping = {
  'get': 'read',
  'head': 'read',
  'post': 'create',
  'put': 'update',
  'delete': 'delete'
};

module.exports = {
  model: function (req) {
    if (!req.options.model) {
      console.log('Model could not be determined');
    }
    return req.options.model;
  },
  method: function (req) {
    return mapping[req.method.toLowerCase()];
  }
};
