/**
 * Determines if an ACL function is applicable to a route
 * Applicable if:
 *  - Method matches
 *  - Route matches (defaults to /, relative to mount point)
 */

function accessTypeMatch (aclType, requestType) {
  return aclType == '*' || aclType.toUpperCase() == requestType.toUpperCase;
}
function routeMatch (aclRoute, requestRoute) {
  // return aclRoute 
  return true; // For now
}
module.exports = function (req, acl, next) {
  var requestMethod = req.method.toUpperCase();
  var requestRoute = req.path;

  var aclRoute = req.path;

  if (accessTypeMatch(acl.accessType, requestMethod) &&
      routeMatch(aclRoute, requestRoute)) {
    next(null, true);
  } else {
    next(null, false);
  }
};
