
var _ = require('lodash');
var async = require('async');

module.exports = function (app) {
  return function (functions, req, res, next) {

    var a = _.map(functions, function (f) {
      var split = f.split(':');
      var functionName = split.shift();

      var func = app.authorization._registry[functionName];
      if (split.length) {
        func = func.apply(null, split);  
      }
      // Curry to apply req, res into the function before next is passed by async
      return _.curry(func)(req, res);
    });

    async.parallel(a, function (err, results) {
      // OR all the results together.
      return next(_.reduce(results, function (running, x) { return running || x; }, false));
    });
  };
};
