
var _ = require('lodash');
var mitch = require('mitch');
var extract = require('./lib/extract');
var loadAuthorizationFunctions = require('./lib/loadAuthorizationFunctions');
var debug = require('debug')('mitch:component:authorization');

module.exports = function(app) {

  var processACLArray = require('./lib/processACLArray')(app);
  // Load Role/RoleMapping models (if RoleMapping is necessary, its really a join-through so not sure yet)

  // Create authorization function registry
  app.authorization = {
    _registry: [],
    _acls: {}
  };

  // Add default authorization functions ($authenticated, $owner)
  app.authorization._registry.$authenticated = function (req, res, next) {
    /**
     * Passes true if there is a currently authenticated user
     */
    next(null, !!(req.user && req.user.id));
  };
  app.authorization._registry.$owner = function (model) {
    return function (req, res, next) {
      /**
       * Passes true if the current model belongs to the current user
       * If there is no current model, we delegate to '$authenticated'
       */
      if (req[model]) {
        next(null, req[model].user_id == req.user.id);
      } else {
        app.authorization._registry.$authenticated(req, res, next);
      }
    };
  };
  // app.authorization._acls.$default = {
  //   create: ['$authenticated'],
  //   read  : ['$authenticated'],
  //   update: ['$authenticated'],
  //   delete: ['$authenticated']
  // };

  var loaded = loadAuthorizationFunctions(app);
  _.extend(app.authorization._registry, loaded.registry);
  _.extend(app.authorization._acls, loaded.acls);

  app._middleware.authorize = function (req, res, next) {
    var model = extract.model(req);
    var method = extract.method(req);

    debug('authorizing: '+model+'.'+method);

    if (app.authorization._acls[model] && app.authorization._acls[model][method]) {
      processACLArray(app.authorization._acls[model][method], req, res, function (authorized) {
        if (authorized) {
          return next();
        } else {
          return res.status(403).json({});
        }
      });
    } else {
      return res.status(403).json({});
    }
  };

  app.registerAuthorizationFunction = function (name, f) {
    app.authorization._registry.name = f;
  };
  app.registerACL = function (obj) {
    _.merge(app.authorization._acl, obj);
  };

  // Todo: Figure out a was to inject this into the 'auth' phase,
  //       but it would have to be after the params are executed (and models loaded)
  
  return true;
};
