# mitch-authorization

Authorization component for mitch apps. 

- Creates authorization function registry
- Based off loopback Role model
- adds mitch.authorize() middleware

- Adds req.user.scope (or req.scope?) object which is an object of the form:
```json
{
  "model": {
    "create": [String]
    "read" 
    "update"
    "delete"
  }
}
```
- Each String must be the name of a function of the format (req, res, next) -> Bool which has been loaded into the authorization registry (app.authorization). If parameters are required to reach this format, params can be added seperated by colons(:). Ie, "$owner:job", will call the $owner function, with the param 'job'.
- Requests can execute the functions in the <model>.<method> array, and 'or' the results to determine access permission

- Adds to .loadAuthorization(acls) to mitch.Router